import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import NavBar from './components/NavBar.jsx';
import Home from './pages/home.jsx';
import Categorias from './pages/categorias.jsx';
import Colecciones from './pages/plp.jsx';
import ListaC from './pages/listaC.jsx';
const App = () => {
  return (
    <Router>
      <div>
        <NavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/categorias" element={<Categorias />} />
          <Route path="/plp" element={<Colecciones />} />
          <Route path="/listaC" element={<ListaC />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;