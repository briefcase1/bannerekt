/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para obtener la informacion de todas las categoiras
 * @returns {object} un objeto con la informacion de las categorias
 */
async function getCategorias() {
    const config = {
        method: 'get',
        url: `https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/categorias`,
        headers: {
            'i-reset': 'true'
        }
    };

    console.log('[getCategorias][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[getCategorias][error]', error);
        });

    console.log('[getCategorias][respuesta]', respuesta);
    return respuesta;
}

/**
 * 
 * Exportaciones
 * 
 */
export default getCategorias;