/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para obtener los banners
 * @returns {object} un objeto con la informacion de los banners
 */
async function getBanners() {
    const config = {
        method: 'get',
        url: `https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/principales`,
        headers: {}
    };

    console.log('[getBanners][options]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[putBanners][error]',error);
        });

    console.log('[getBanners][respuesta]', respuesta);
    return respuesta;


}

/**
 * 
 * Exportaciones
 * 
 */
export default getBanners;