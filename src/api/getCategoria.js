/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para obtener la informacion de la categoria 
 * @param {string} id id de la categoria a buscar 
 * @returns {object} un objeto con la informacion de la categoria
 */
async function getCategoria(id) {
    const config = {
        method: 'get',
        url: `https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/categoria?id=${id}`,
        headers: {
            'i-reset': 'true'
        }
    };

    console.log('[getCategorias][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[putCategoria][error]', error);
        });

    console.log('[getCategoria][respuesta]', respuesta);
    return respuesta;
}

/**
 * 
 * Exportaciones
 * 
 */
export default getCategoria;