/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para obtener la lista de categorias
 * @returns {objeto} un objeto con la informacion de lista de categorias
 */
async function getCategoriasLista() {
    const config = {
        method: 'get',
        url: `https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/categorias/lista`,
        headers: {
            'i-reset': 'true'
        }
    };

    console.log('[getCategoriasLista][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[getCategoriasLista][error]', error);
        });

    console.log('[getCategoriasLista][respuesta]', respuesta);

    return respuesta;


}

/**
 * 
 * Exportaciones
 * 
 */
export default getCategoriasLista;