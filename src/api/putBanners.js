/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para actualizar los datos de los banners
 * @param {object} infoB 
 * @returns 
 */
async function putBanners(infoB) {
    try {
        let info = {
            data: infoB
        }

        let data = JSON.stringify(info);

        console.log('[putBanners][data]', data);

        const config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/actualizar/principales',
            headers: {
                'Content-Type': 'application/json',
                'i-reset': 'true'
            },
            data
        };

        console.log('[putBanners][config]', config);

        const respuesta = await axios(config)
            .then(function (response) {
                return response.data;
            })
            .catch(function (error) {
                console.log('[putBanners][error]', error);
                throw error;
            });

        console.log('[putBanners][respuesta]', respuesta);
        return respuesta;
    } catch (e) {
        console.error('[putBanners][error]', e);
        throw e;
    }

}

/**
 * 
 * Exportaciones
 * 
 */
export default putBanners;