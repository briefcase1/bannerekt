/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para obtener los datos de las colecciones
 * @returns {object} retornar un objeto con los datos de las colecciones
 */
async function getColecciones() {
    const config = {
        method: 'get',
        url: `https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/colecciones`,
        headers: {
            'i-reset': 'true'
        }
    };

    console.log('[getColecciones][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[putColecciones][error]', error);
        });

    console.log('[getColecciones][respuesta]', respuesta);
    return respuesta;
}


/**
 * 
 * Exportaciones
 * 
 */
export default getColecciones;