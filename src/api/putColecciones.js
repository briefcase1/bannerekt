/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * 
 * Funcion para actualizar las colecciones
 * @param {String} tipoOperacion tipo de operacion que se va a realizar 
 * @param {String} idColeccion id de la coleccion a cambiar
 * @param {String} idSubColeccion id de la subcoleccion 
 * @param {String} nombre nombre de la coleccion
 * @param {String} url url de la coleccion
 * @returns 
 */
async function putColecciones(tipoOperacion, idColeccion, idSubColeccion, nombre, url) {

    let info = {
        "tipoOperacion": tipoOperacion,
        "idColeccion": idColeccion,
        "idSubColeccion": idSubColeccion,
        "url": url,
        "nombre": nombre
    };

    let data = JSON.stringify(info);

    console.log('[putColecciones][data]', data);

    const config = {
        method: 'put',
        maxBodyLength: Infinity,
        url: 'https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/actualizar/colecciones',
        headers: {
            'Content-Type': 'application/json',
            'i-reset': 'true'
        },
        data
    };

    console.log('[putColecciones][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[putColecciones][error]', error);
        });

    console.log('[putColecciones][respuesta]', respuesta);
    return respuesta;


}

export default putColecciones;