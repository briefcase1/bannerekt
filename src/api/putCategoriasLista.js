/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * 
 * Funcion para actualizar la lista de categorias
 * @param {String} tipoOperacion tipo de operacion a realizar 
 * @param {String} id id de categoria a actualizar 
 * @param {String} nombre nombre de la categoria
 * @param {String} url url de identificacion de la categoria
 * @returns
 *  
 */
async function putCategoriasLista(tipoOperacion, id, nombre, url) {

    let info = {
        "tipoOperacion": tipoOperacion,
        "id": id,
        "url": url,
        "nombre": nombre
    }

    let data = JSON.stringify(info);

    console.log('[putCategoriasLista][data]', data);

    const config = {
        method: 'put',
        maxBodyLength: Infinity,
        url: 'https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/actualizar/categorias/lista',
        headers: {
            'Content-Type': 'application/json',
            'i-reset': 'true'
        },
        data
    };

    console.log('[putCategoriasLista][config]', config);

    const respuesta = await axios(config)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            console.log('[putCategoriasLista][error]', error);
        });
    console.log('[putCategoriasLista][respuesta]', respuesta);
    return respuesta;
}

export default putCategoriasLista;