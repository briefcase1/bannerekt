/**
 * 
 * Importaciones
 * 
 */
import axios from 'axios';

/**
 * Funcion para actualizar los datos de la categoria
 * @param {Number} posicion posicion de la cual se va a actualizar 
 * @param {String} tipoOperacion tipo de operacion que se va a realizar
 * @param {String} idCategoria idCategoria que se va a actualizar 
 * @param {Object} datos un objeto con la informacion de la categoria a actualizar 
 * @param {String} nombre nombre de la configuracion es el titulo que se visualiza  
 * @returns 
 */
async function putCategorias(posicion, tipoOperacion, idCategoria, datos, nombre) {
  let info = {
    "posicion": posicion,
    "tipoOperacion": tipoOperacion,
    "categoria": {
      "id": idCategoria,
      "nombre": nombre,
      "url": ""
    },
    "datos": datos
  };

  let data = JSON.stringify(info);

  console.log('[putCategorias][data]', data);

  const config = {
    method: 'put',
    maxBodyLength: Infinity,
    url: 'https://ecomm-dev.appelektradev.com/v1/ekt/richie/banners/actualizar/categorias',
    headers: {
      'Content-Type': 'application/json',
      'i-reset': 'true'
    },
    data
  };

  console.log('[putCategorias][config]', config);

  const respuesta = await axios(config)
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log('[putCategorias][error]', error);
    });

  console.log('[putCategorias][respuesta]', respuesta);
  return respuesta;
}

export default putCategorias;