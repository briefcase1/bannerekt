import ColumnaFormulario from '../components/columnaFormulario';
import ColumnaVista from '../components/columnaVista';
import { Grid } from '@mui/material';


function Home() {
    return (
        <>
            <br />
            <Grid container spacing={2}>
                <ColumnaFormulario />
                <ColumnaVista />
            </Grid>
        </>
    )
}

export default Home
