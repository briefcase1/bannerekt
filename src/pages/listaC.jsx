import ColumnaFormularioCatL from '../components/columnaFormularioLista';
import ColumnaVistaCatL from '../components/columnaVistaListaC';
import { Grid } from '@mui/material';


function ListaC() {
    return (
        <>
            <br />
            <Grid container spacing={2}>
                <ColumnaFormularioCatL />
                <ColumnaVistaCatL />
            </Grid>
        </>
    )
}

export default ListaC
