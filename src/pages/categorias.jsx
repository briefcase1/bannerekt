import ColumnaFormularioCat from '../components/columnFormularioCategorias';
import ColumnaVistaCategorias from '../components/columnaVistaCategorias';
import { Grid } from '@mui/material';


function Categorias() {
    return (
        <>
            <br />
            <Grid container spacing={2}>
                <ColumnaFormularioCat />
                <ColumnaVistaCategorias />
            </Grid>
        </>
    )
}

export default Categorias
