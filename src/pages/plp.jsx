import ColumnaFormularioColecciones from '../components/columnaFormularioColecciones';
import ColumnaVistaColecciones from '../components/columnaVistaColecciones';
import { Grid } from '@mui/material';


function Colecciones() {
    return (
        <>
            <br />
            <Grid container spacing={2}>
                <ColumnaFormularioColecciones />
                <ColumnaVistaColecciones />
            </Grid>
        </>
    )
}

export default Colecciones
