import create from 'zustand';

const useDataColecciones = create((set) => ({
  sharedData: '',
  setSharedData: (newData) => set({ sharedData: newData }),
}));




export default useDataColecciones;