import create from 'zustand';

const useDataStoreFC = create((set) => ({
  sharedData: '',
  setSharedData: (newData) => set({ sharedData: newData }),
}));




export default useDataStoreFC;