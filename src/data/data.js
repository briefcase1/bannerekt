import create from 'zustand';

const useDataStore = create((set) => ({
  sharedData: '',
  setSharedData: (newData) => set({ sharedData: newData }),
}));




export default useDataStore;