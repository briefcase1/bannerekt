import create from 'zustand';

const useDataBanners = create((set) => ({
  sharedData: '',
  setSharedData: (newData) => set({ sharedData: newData }),
}));




export default useDataBanners;