import { create } from 'zustand'

const formulario = create((set) => ({
    nombre: null,
    filtro: '',
    ordenamiento: '',
    categorias: null,
    caracteristicas: '',
    posicion: 1,
    urlImagen: '',
    seccionId: 1,
    nombreSeccion: '',
    title: '',
    seccion: '1',
    idCategoria: null,
    ids: null,
    colecciones: null,
    categoriasCombinadas: null,
    titulo: '',
    updateTitle: (newTitle) => set({ title: newTitle }),


}));

export default formulario