import { create } from 'zustand'

const formulario = create((set) => ({
    filtro: '',
    ordenamiento: '',
    categorias: null,
    caracteristicas: '',
    seccion: '1',
    tipoBanner: '1',
    posicion: 1,
    urlImagen: '',
    colecciones: null,
    categoriasCombinadas: null,
    seccionId: 1,
    nombreSeccion: '',
    titulo: '',

    updateTitle: (newTitle) => set({ title: newTitle }),


}));

export default formulario