/* eslint-disable react/prop-types */
/**
 * 
 * Importaciones
 * 
 */
import { CardMedia, Card, CardContent, Button } from '@mui/material';
import useDataStore from '../data/data';

/**
 * 
 * Funcion para visualizar los datos del carrusel
 * 
 */
const BannerComponent = ({ criteriosConsulta, urlImagen, posicion }) => {

    /**
     * Funcion para actualizar los datos de useDataStore(Datos del formulario de banners Home)
     */
    const setSharedData = useDataStore((state) => state.setSharedData);

    /**
     * Manejador al darle clic a la imagen ó el botón de modificar
     */
    const handleImageClick = () => {
        const newCriteriosConsulta = {
            ...criteriosConsulta, //criterios de busquedas del banner
            posicion: posicion, // la posicion actual del banner
            urlImagen: urlImagen, // url de la imagen
            seccionPos: '1', // posicion de seccion [principales, secundarios, populares]
        };

        console.log('[BannersComponent][handleImageClick][criterops]', newCriteriosConsulta);
        setSharedData(newCriteriosConsulta);
    };

    /**
     * 
     * Configuracion de la card de la imagen del banner del carrusel
     * 
     */
    return (
        <div>
            <Card>
                <CardMedia
                    component="img"
                    alt="Imagen"
                    height="100%"
                    image={urlImagen}
                    onClick={handleImageClick}
                    style={{ cursor: 'pointer' }}
                />
                <CardContent>
                    <Button variant="contained" color="success" onClick={handleImageClick}>
                        Modificar
                    </Button>
                </CardContent>
            </Card>
        </div>
    );
};

/**
 * 
 * Exportaciones
 * 
 */
export default BannerComponent;