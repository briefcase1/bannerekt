/* eslint-disable no-case-declarations */
/**
 * 
 * 
 * Importaciones
 * 
 */
import React, { useState } from "react";
import { useEffect } from 'react';
import '../App.css';
import {
    TextField, Button, Grid, Paper, FormControl, MenuItem, InputLabel, Select
    , Typography
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import categoriasData from '../resources/categorias.json';
import formulario from '../store/formularios';
import useDataStore from '../data/data';
import categoriasJson from '../resources/categoriasApp.json';
import getBanners from '../api/getBanners';
import putBanners from '../api/putBanners';
import useDataBanners from '../data/dataBanners';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import getColecciones from '../api/getColecciones';
import getCategorias from '../api/getCategorias';
import CircularProgress from '@mui/material/CircularProgress';

const categoriasDepartamentos = categoriasJson;

/**
 * Funcion para retornar el formulario de llenado de datos de home
 * @returns vista de formulario
 */
const ColumnaFormulario = () => {



    /**
     * 
     * Constante para almacenar datos de los banners
     */
    const [banners, setBanners] = useState([]);
    const [categoriasSelect, setCategoriasSelect] = useState([]);
    const [coleccionesSelect, setColeccionesSelect] = useState([]);
    const [loader, setLoader] = useState(false);
    const [loaderMod, setLoaderMod] = useState(false);
    const [loaderDel, setLoaderDel] = useState(false);
    /**
     * 
     * Se utiliza el useEffect al iniciar el componente cargue la informacion de los banners  
     * 
     */
    React.useEffect(() => {
        datos();
    }, [])


    /**
     * 
     * Constantes para guardar la informacion de los banners y ser compartida con el 
     * componente vista para visualizar los cambios
     */
    const setSharedData = useDataBanners((state) => state.setSharedData);
    const sharedDataBanners = useDataBanners((state) => state.sharedData);

    /**
     * 
     * Funcion conseguir los datos  de los banners
     * 
     */
    const datos = async () => {
        const res = await getBanners();
        console.log('[ColumnaFormulario][resultado][banners]', res.resultado);
        setBanners(res.resultado);
        /////////////////
        const resCat = await getCategorias();
        console.log('[ColumnaFormulario][resultado][categorias]', resCat);
        setCategoriasSelect(resCat.resultado);
        //////////////
        let resColec = await getColecciones();
        console.log('[ColumnaFormulario][respuesta][colecciones]', resColec);
        resColec = resColec.resultado.colecciones.map(item => {
            return {
                "label": `${item.subcolecciones[0].id} [${item.nombre} - ${item.url}]`,
                "value": item.subcolecciones[0].id
            }
        });
        setColeccionesSelect(resColec);
    }

    /**
     * 
     * Destructuracion de datos de formularioen constantes
     * 
     */
    const { filtro, ordenamiento, categorias
        , caracteristicas, seccion, tipoBanner, posicion, urlImagen,
        seccionId, nombreSeccion, colecciones, categoriasCombinadas } = formulario(state => state);



    /**
     * 
     * Banderas para mostrar distintas opciones del formulario dependiendo de la
     * seccion si es un producto o secciones
     * 
     */
    const [showIdSection, setShowIdSection] = useState(false);
    const [showNombreSetccion, setShowNombreSetccion] = useState(false);
    const [showFiltro, setShowFiltro] = useState(true);
    const [showOrdenamiento, setShowOrdenamiento] = useState(true);


    /**
     * 
     * Constante de los datos del formulario para realizar los cambios
     * 
     */
    const [formData, setFormData] = useState({
        filtro: filtro, //producto.filtro
        ordenamiento: ordenamiento, //producto.ordenamiento
        categorias: categorias, //No existe
        caracteristicas: caracteristicas, // producto.caracteristicas ó seccion.caracteristicas
        seccion: seccion, //Tipo de seccion productos o seccion
        tipoBanner: tipoBanner, // Populares, Hot, Principales
        posicion: posicion, //posicion del banners
        urlImagen: urlImagen, // imagen del banner
        seccionId: seccionId, // seccion.id
        nombreSeccion: nombreSeccion, // seccion.nombre
        colecciones: colecciones, //conjunto de colecciones de PLP
        categoriasCombinadas: categoriasCombinadas // conjunto de categorias combinas
    });

    /**
     * 
     * Asignacion de la data compartida entre los banners y formularios, 
     * la info es la que se manda de los componentes card
     * 
     */
    const sharedData = useDataStore((state) => state.sharedData);
    console.log('[ColumnaVista][sharedData]', sharedData);

    /**
     * Funcion para actualizar los datos de useDataStore(Datos del formulario de banners Home)
     */
    const setSharedDataFormulario = useDataStore((state) => state.setSharedData);

    /**
     * 
     * Use Effect para actualizar informacion en cada cambio de datos
     */
    useEffect(() => {

        //si los datos compartidos de los banner no existen buscamos  la informacion de nuevo
        if (sharedDataBanners === '') {
            datos();
        } else {
            //En caso de existir los almacenmos en la constante de los banner
            setBanners(sharedDataBanners);
        }

        /**
         * Si hubo un cambio en el sharedData (informacion desde las card se actualizan los datos 
         * del formulario)
         */
        setFormData({
            filtro: sharedData?.producto?.filtro ?? '',
            ordenamiento: sharedData?.producto?.ordenamiento ?? '',
            categorias: null,
            caracteristicas: sharedData?.id === 1 ? sharedData?.producto?.caracteristicas : sharedData?.seccion?.caracteristicas ?? '',
            seccion: sharedData?.id ?? '1',
            tipoBanner: sharedData?.seccionPos ?? '1',
            posicion: sharedData?.posicion ?? 0,
            urlImagen: sharedData?.urlImagen ?? '',
            nombreSeccion: sharedData?.seccion?.nombre ?? '',
            seccionId: sharedData?.seccion?.id ?? seccionId ?? '1',
            colecciones: null,
            categoriasCombinadas: null
        })

        console.log('[ColumnaFormulario][seccion][boolean]', sharedData.id === 2);

        //dependiendo de tipo de armado producto (1) o seccion (2) se muestran u ocultan campos
        // del formualrio
        if (sharedData.id != 2) {
            setShowIdSection(false);
            setShowNombreSetccion(false);
            setShowFiltro(true);
            setShowOrdenamiento(true);
        } else {
            setShowIdSection(true);
            setShowNombreSetccion(true);
            setShowFiltro(false);
            setShowOrdenamiento(false);
        }
    }, [sharedData, sharedDataBanners]);


    /**
     * 
     * Funcion para actualizar los datos del formualrio 
     * 
     */
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        console.log('[ColumnaVista][handleInputChange][name]', name);
        console.log('[ColumnaVista][handleInputChange][value]', value);
        setFormData({
            ...formData,
            [name]: value
        });
    };

    /**
     * 
     * Funcion para cambiar el valor de la posicion del banners
     * 
     */
    const handlePosicionChange = (event) => {
        const { name, value } = event.target;
        console.log('[ColumnaVista][handlePosicionChange][value]', value)
        console.log('[ColumnaVista][handlePosicionChange][name]', name)
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value
        }));

    }

    /**
     * 
     * Funcion que dependiendo del tipo de busqueda cambiara las opciones
     * 
     */
    const handleTipoChange = (event, newValue) => {

        setFormData({
            ...formData,
            seccion: newValue.props.value
        });

        if (formData.seccion == '2') {
            setShowIdSection(false);
            setShowNombreSetccion(false);
            setShowFiltro(true);
            setShowOrdenamiento(true);
        } else {
            setShowIdSection(true);
            setShowNombreSetccion(true);
            setShowFiltro(false);
            setShowOrdenamiento(false);
        }
    };


    /**
     * 
     * Funcion para cambiar el tipo de seccion que
     * es el id  del 1 al 7
     */
    const handleTipoIdSeccion = (event, newValue) => {
        setFormData({
            ...formData,
            seccionId: newValue.props.value
        });
        console.log('[handleTipoSeccion][id]', newValue.props.value)
    }


    /**
     * 
     * Funcion para cambiar el tipo de banner si es popular
     * principal o secundario
     */
    const handleBannerChange = (event, newValue) => {
        setFormData({
            ...formData,
            tipoBanner: newValue.props.value
        });
    };

    /**
    * 
    * Funcion seleccionar el valor de una categoria combina
    */
    const handleCategoriasCombinadasChange = (event, newValue) => {
        console.log('[ColumnaFormulario][handleCategoriasCombinadasChange][newValue]', newValue);
        setFormData({
            ...formData,
            categoriasCombinadas: newValue
        });
    };

    /**
     * 
     * Funcion para cambiar el valor de categorias
     * que se seleccionan de todas las que esten disponibles para sacar
     * los valores para los tipos de busquedas
     * 
     */
    const handleCategoriasChange = (event, newValue) => {
        console.log('[ColumnaVista][handleCategoriasChange][newValue]', newValue);
        setFormData({
            ...formData,
            categorias: newValue
        });
    };

    /**
     * 
     * Funcion para cambiar el valor de las colecciones
     * 
     */
    const handleColeccioneChange = (event, newValue) => {
        console.log('[ColumnaVista][handleCategoriasChange][newValue]', newValue);
        setFormData({
            ...formData,
            colecciones: newValue
        });
    };
    /**

    /**
     * 
     * Funcion para crear un elemento de un banner
     * 
     */
    const handleSubmit = async (event) => {
        try {
            setLoader(true);
            event.preventDefault();

            console.log('[ColumnaVista][handleSubmit][creando] . .');
            console.log('[ColumnaVista][hanldeSubmit][formData]', formData);

            let body = {
                criteriosConsulta: {
                    id: '',
                    nombre: '',
                    producto: {
                        filtro: '',
                        ordenamiento: '',
                        caracteristicas: '',
                    },
                    seccion: {
                        id: '',
                        nombre: '',
                        caracteristicas: '',
                    },
                },
                urlImagen: '',
            };

            const band = formData?.categorias?.value ? true : false;
            //Dependiendo del tipo de busqueda se consigue el body de acuerdo 
            // a la busqueda
            switch (String(formData.seccion)) {
                case '1':
                    getBodyProductos(formData, body, band);
                    break;
                case '2':
                    getBodySecciones(formData, body, band);
                    break;
            }

            console.log('[ColumnaVista][handleSubmit][body]', body);

            //Dependiendo del tipo de banner popular, popular o secundario se agregara
            switch (formData.tipoBanner) {
                case '1':

                    let auxBanners = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion = auxBanners?.anuncios?.principales.slice(); // Crear una copia del array
                    seccion.splice(formData.posicion, 0, body) // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners = {
                        ...auxBanners,
                        anuncios: {
                            ...auxBanners.anuncios,
                            principales: seccion,
                        },
                    };

                    setBanners(auxBanners); // Actualiza la informacion de los banner de manera local
                    await putBanners(auxBanners); // Funcion para actualizar el JSON en el S3
                    setSharedData(auxBanners); //Actualiza la informacion compartida al componente vista para actualizar la vista

                    break;


                case '2':

                    let auxBanners2 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion2 = auxBanners2?.anuncios?.secundarios.slice(); // Crear una copia del array
                    seccion2.splice(formData.posicion, 0, body) // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners2 = {
                        ...auxBanners2,
                        anuncios: {
                            ...auxBanners2.anuncios,
                            secundarios: seccion2,
                        },
                    };
                    setBanners(auxBanners2); // Actualiza la informacion de los banner de manera local
                    await putBanners(auxBanners2); // Funcion para actualizar el JSON en el S3
                    setSharedData(auxBanners2); //Actualiza la informacion compartida al componente vista para actualizar la vista
                    break;

                case '3':

                    let auxBanners3 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion3 = auxBanners3?.anuncios?.categoriasPopulares.slice(); // Crear una copia del array
                    seccion3.splice(formData.posicion, 0, body) // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners3 = {
                        ...auxBanners3,
                        anuncios: {
                            ...auxBanners3.anuncios,
                            categoriasPopulares: seccion3,
                        },
                    };

                    setBanners(auxBanners3); // Actualiza la informacion de los banner de manera local
                    await putBanners(auxBanners3); // Funcion para actualizar el JSON en el S3
                    setSharedData(auxBanners3); //Actualiza la informacion compartida al componente vista para actualizar la vista

                    break;
            }
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
            setSharedDataFormulario({});
        } catch (e) {
            console.error('[ColumnaVista][handleSubmit][error]', e);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false)
        }
    };


    /**
     * 
     * Funcion para eliminar un elemento de banner
     * 
     */
    const handleEliminar = async (event) => {
        try {
            event.preventDefault();
            setLoaderDel(true);
            console.log('[ColumnaVista][handleEliminar][eliminando] . .');
            console.log('[ColumnaVista][hanldeEliminar][formData]', formData);

            //Dependiendo del tipo de banner popular, popular o secundario se agregara
            switch (formData.tipoBanner) {

                case '1':

                    let auxBanners = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion = auxBanners?.anuncios?.principales.slice(); // Crear una copia del array

                    seccion.splice(Number(formData.posicion), 1); // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners = {
                        ...auxBanners,
                        anuncios: {
                            ...auxBanners.anuncios,
                            principales: seccion,
                        },
                    };

                    setBanners(auxBanners);
                    await putBanners(auxBanners);
                    setSharedData(auxBanners);
                    break;

                case '2':

                    let auxBanners2 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion2 = auxBanners2?.anuncios?.secundarios.slice(); // Crear una copia del array

                    seccion2.splice(Number(formData.posicion), 1); // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners2 = {
                        ...auxBanners2,
                        anuncios: {
                            ...auxBanners2.anuncios,
                            secundarios: seccion2,
                        },
                    };

                    setBanners(auxBanners2);
                    await putBanners(auxBanners2);
                    setSharedData(auxBanners2);
                    break;

                case '3':

                    let auxBanners3 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion3 = auxBanners3?.anuncios?.categoriasPopulares.slice(); // Crear una copia del array

                    seccion3.splice(Number(formData.posicion), 1); // Usar insertAt para obtener un nuevo array con la inserción
                    auxBanners3 = {
                        ...auxBanners3,
                        anuncios: {
                            ...auxBanners3.anuncios,
                            categoriasPopulares: seccion3,
                        },
                    };

                    setBanners(auxBanners3);
                    await putBanners(auxBanners3);
                    setSharedData(auxBanners3);

                    break;
            }
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedDataFormulario({});
            setLoaderDel(false);
        } catch (e) {
            console.error('[ColumnaVista][handleEliminar][error]', e);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoaderDel(false);
        }
    }


    /**
     * 
     * Funcion para modificar los datos de un banner
     */
    const handleModificar = async (event) => {
        try {
            event.preventDefault();
            setLoaderMod(true);
            console.log('[ColumnaVista][handleModificar][creando] . .');
            console.log('[ColumnaVista][handleModificar][formData]', formData);

            let body = {
                criteriosConsulta: {
                    id: '',
                    nombre: '',
                    producto: {
                        filtro: '',
                        ordenamiento: '',
                        caracteristicas: '',
                    },
                    seccion: {
                        id: '',
                        nombre: '',
                        caracteristicas: '',
                    },
                },
                urlImagen: '',
            };

            const band = formData?.categorias?.value ? true : false;
            switch (String(formData.seccion)) {
                case '1':
                    getBodyProductos(formData, body, band);
                    break;
                case '2':
                    getBodySecciones(formData, body, band);
                    break;
            }

            console.log(body);
            switch (formData.tipoBanner) {
                case '1':

                    let auxBanners = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion = auxBanners?.anuncios?.principales.slice(); // Crear una copia del array

                    seccion[Number(formData.posicion)] = body;
                    auxBanners = {
                        ...auxBanners,
                        anuncios: {
                            ...auxBanners.anuncios,
                            principales: seccion,
                        },
                    };

                    setBanners(auxBanners);
                    await putBanners(auxBanners);
                    setSharedData(auxBanners);
                    break;

                case '2':

                    let auxBanners2 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion2 = auxBanners2?.anuncios?.secundarios.slice(); // Crear una copia del array

                    seccion2[Number(formData.posicion)] = body;
                    auxBanners2 = {
                        ...auxBanners2,
                        anuncios: {
                            ...auxBanners2.anuncios,
                            secundarios: seccion2,
                        },
                    };

                    setBanners(auxBanners2);
                    await putBanners(auxBanners2);
                    setSharedData(auxBanners2);
                    break;

                case '3':

                    let auxBanners3 = { ...banners }; // Crear una copia superficial del objeto banners
                    let seccion3 = auxBanners3?.anuncios?.categoriasPopulares.slice(); // Crear una copia del array

                    seccion3[Number(formData.posicion)] = body;
                    auxBanners3 = {
                        ...auxBanners3,
                        anuncios: {
                            ...auxBanners3.anuncios,
                            categoriasPopulares: seccion3,
                        },
                    };

                    setBanners(auxBanners3);
                    await putBanners(auxBanners3);
                    setSharedData(auxBanners3);

                    break;

            }
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedDataFormulario({});
            setLoaderMod(false);
        } catch (e) {
            console.error('[ColumnaVista][handleModificar][error]', e);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    }


    /**
     * 
     * Funcion para obtener el body de acuerdo a la busqueda de productos
     * se tiene una bandera para indicar el armado es en abse a una cateoria
     */
    function getBodyProductos(formData, body, crear) {
        if (crear) {

            console.log('[ColumnaVista][getBodyProducts][crear]', crear);

            let base = 'category-';
            let baseCat = ''

            const valorCategorias = formData.categorias.value.split('---');
            const rutas = valorCategorias[1].split('/');


            for (let x = 0; x < rutas.length; x++) {
                baseCat = `${baseCat}${base}${x + 1}/${rutas[x]}/`;
            }

            //Armado
            body['criteriosConsulta']['id'] = 1;
            body['criteriosConsulta']['nombre'] = 'productos';
            body['criteriosConsulta']['producto']['caracteristicas'] = baseCat;
            body['criteriosConsulta']['producto']['ordenamiento'] = '';
            body['criteriosConsulta']['producto']['filtro'] = '';
            body['criteriosConsulta']['seccion']['id'] = 0;
            body['criteriosConsulta']['seccion']['nombre'] = 'NA';
            body['criteriosConsulta']['seccion']['caracteristicas'] = 'NA';
            body['urlImagen'] = formData.urlImagen;

        } else {

            //Armado
            body['criteriosConsulta']['id'] = 1;
            body['criteriosConsulta']['nombre'] = 'productos';
            body['criteriosConsulta']['producto']['caracteristicas'] = formData.caracteristicas ?? '';
            body['criteriosConsulta']['producto']['ordenamiento'] = formData.ordenamiento ?? '';
            body['criteriosConsulta']['producto']['filtro'] = formData.filtro ?? '';
            body['criteriosConsulta']['seccion']['id'] = 0;
            body['criteriosConsulta']['seccion']['nombre'] = 'NA';
            body['criteriosConsulta']['seccion']['caracteristicas'] = 'NA';
            body['urlImagen'] = formData.urlImagen;
        }
    }

    /** 
     * 
     * Funcion para obtener el body si la busqueda es de seccion
     * se tiene una bandera para indicar el armado es en abse a una cateoria
     * 
     */
    function getBodySecciones(formData, body, crear) {
        if (crear) {

            let baseCat = '';
            let baseNombre = ''

            const valorCategorias = formData.categorias.value.split('---');
            console.log(valorCategorias[1]);
            const rutas = valorCategorias[1].split('/');

            for (let x = 0; x < rutas.length; x++) {
                if (x === 0) {
                    let catgeroriaAux = categoriasDepartamentos.categorias.find((element) => quitarAcentos(element.nombre.toLowerCase()) === rutas[x] && element.nivel === 1);
                    baseNombre = baseNombre + catgeroriaAux['nombre'] + '/';
                    baseCat = baseCat + catgeroriaAux['id'] + '/';
                } else {
                    let catgeroriaAux = categoriasDepartamentos.categorias.find((element) => quitarAcentos(element.nombre.toLowerCase()) === rutas[x] && element.id === valorCategorias[2]);
                    baseNombre = baseNombre + catgeroriaAux['nombre'] + '/';
                    baseCat = baseCat + catgeroriaAux['id'] + '/';
                }
            }

            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = baseNombre.slice(0, baseNombre.length - 1);
            body['criteriosConsulta']['seccion']['caracteristicas'] = baseCat.slice(0, baseCat.length - 1);
            body['urlImagen'] = formData.urlImagen;
        } else if (formData.seccionId == 1) {
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = 'colecciones';
            body['criteriosConsulta']['seccion']['caracteristicas'] = formData?.colecciones?.value ?? formData.caracteristicas;
            body['urlImagen'] = formData.urlImagen;
        } else if (formData.seccionId == 3) {

            const category = formData?.categoriasCombinadas?.label?.split('--') ?? [];
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = !category[1] ? formData.nombreSeccion : category[1];
            body['criteriosConsulta']['seccion']['caracteristicas'] = !category[0] ? formData.caracteristicas : category[0];
            body['urlImagen'] = formData.urlImagen;

        } else {
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = formData.nombreSeccion;
            body['criteriosConsulta']['seccion']['caracteristicas'] = formData.caracteristicas;
            body['urlImagen'] = formData.urlImagen;
        }
    }



    //Funcion para normalizar las cadenas quitando los acentos
    function quitarAcentos(texto) {
        return texto.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }

    return (
        <Grid item xs={6}>
            <ToastContainer />
            <Paper elevation={3} style={{ padding: 20 }}>
                <Typography variant="h5" align="center" gutterBottom>
                    Datos de entrada
                </Typography>
                <form onSubmit={handleSubmit}>
                    <FormControl fullWidth>
                        <InputLabel id="tipo-label">Tipo</InputLabel>
                        <Select
                            labelId="tipo-label"
                            id="tipo-select"
                            value={formData.seccion}
                            label="Tipo"
                            onChange={handleTipoChange}
                        >
                            <MenuItem value="1">Productos</MenuItem>
                            <MenuItem value="2">Seccion</MenuItem>
                        </Select>
                    </FormControl>
                    <Autocomplete
                        id="categorias"
                        options={categoriasData}
                        getOptionLabel={(option) => option.label}
                        value={formData.categorias}
                        onChange={handleCategoriasChange}
                        renderInput={(params) => <TextField {...params} label="Categorias" margin="normal" />}
                    />
                    <FormControl fullWidth>
                        <InputLabel id="banner-label">Tipo de Banner</InputLabel>
                        <Select
                            labelId="banner-label"
                            id="banner-select"
                            value={formData.tipoBanner}
                            label="Tipo de Banner"
                            onChange={handleBannerChange}
                        >
                            <MenuItem value="1">Principales</MenuItem>
                            <MenuItem value="2">Lo más Hot </MenuItem>
                            <MenuItem value="3">Populares</MenuItem>

                        </Select>
                    </FormControl>
                    <TextField
                        label="Posición"
                        name="posicion"
                        type="number"
                        fullWidth
                        value={formData.posicion}
                        onChange={handlePosicionChange}
                        margin="normal"
                    />

                    {showIdSection &&
                        (<FormControl fullWidth>
                            <InputLabel id="tipo-id-seccion">Seccion ID</InputLabel>
                            <Select
                                labelId="tipo-id-seccion"
                                id="tipo-id-seccion"
                                value={formData.seccionId}
                                label="ID de seccion"
                                onChange={handleTipoIdSeccion}
                            >
                                <MenuItem value="1">PLP Colecciones</MenuItem>
                                <MenuItem value="2">Categorias</MenuItem>
                                <MenuItem value="3">Categorias Combinadas</MenuItem>
                                <MenuItem value="7">PDP</MenuItem>
                            </Select>
                        </FormControl>)}


                    {Number(formData.seccionId) === 1 && Number(formData.seccion) === 2 && (<Autocomplete
                        id="colecciones"
                        options={coleccionesSelect}
                        getOptionLabel={(option) => option.label}
                        value={formData.colecciones}
                        onChange={handleColeccioneChange}
                        renderInput={(params) => <TextField {...params} label="Colecciones" margin="normal" />}
                    />)}

                    {Number(formData.seccionId) === 3 && Number(formData.seccion) === 2 && (<Autocomplete
                        id="categoriasCombinadas"
                        options={categoriasSelect}
                        getOptionLabel={(option) => option?.label}
                        value={formData.categoriasCombinadas}
                        onChange={handleCategoriasCombinadasChange}
                        renderInput={(params) => <TextField {...params} label="Categorias Combinadas" margin="normal" />}
                    />)}

                    {showNombreSetccion && (<TextField
                        label="Nombre de seccion"
                        name="nombreSeccion"
                        fullWidth
                        value={formData.nombreSeccion}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    )}

                    {showFiltro && (
                        <TextField
                            label="Filtro"
                            name="filtro"
                            fullWidth
                            value={formData.filtro}
                            onChange={handleInputChange}
                            margin="normal"
                        />

                    )}

                    {showOrdenamiento && (<TextField
                        label="Ordenamiento"
                        name="ordenamiento"
                        fullWidth
                        value={formData.ordenamiento}
                        onChange={handleInputChange}
                        margin="normal"
                    />)}

                    <TextField
                        label="Caracteristicas"
                        name="caracteristicas"
                        fullWidth
                        value={formData.caracteristicas}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    <TextField
                        label="Imagen"
                        name="urlImagen"
                        fullWidth
                        value={formData.urlImagen}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    <Button fullWidth disabled={loader} type="submit" variant="contained" color="primary">
                        {loader ? <CircularProgress size={24} color="inherit" /> : 'Crear'}
                    </Button>
                </form>
                <br />
                <Button fullWidth onClick={handleModificar} variant="contained" color="secondary" >
                    {loaderMod ? <CircularProgress size={24} color="inherit" /> : 'Modificar'}
                </Button>
                <br />
                <br />
                <Button fullWidth onClick={handleEliminar} variant="contained" color="error">
                    {loaderDel ? <CircularProgress size={24} color="inherit" /> : 'Eliminar'}
                </Button>
            </Paper>
        </Grid>
    );
}

export default ColumnaFormulario;