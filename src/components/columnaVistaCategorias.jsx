/**
 * 
 * Importaciones
 * 
 */
import React, { useState } from "react";
import '../App.css';
import { Grid, Paper, Typography } from '@mui/material';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import CategoriasComponent from './cardCategorias';
import getCategoria from '../api/getCategoria';
import useDataCategorias from '../data/dataCategoria';
import Loader from './Loader';

/**
 * 
 * Funcion que regresa la vista con las secciones
 * 
 */
const ColumnaVistaCategorias = () => {

    /**
     * 
     * Constantes globales
     * 
     */
    const [categorias, setCategorias] = useState([]);
    const [loader, setLoader] = useState(true);

    /**
     * 
     * constante para utilizar los datos que vienen desde el formulario
     * 
     */
    const sharedData = useDataCategorias((state) => state.sharedData);

    console.log('[columnaVistaCategorias][sharedData]', sharedData);

    React.useEffect(() => {
        if (sharedData === '') {
            datos();
        } else {
            setCategorias(sharedData);
        }
        setLoader(false);
    }, [sharedData])


    //Funcion para obtener los datos de la categoria selccionada
    const datos = async () => {
        console.log('[columnaVistaCategorias][datos] . .');
        const res = await getCategoria(1);
        console.log('[columnaVistaCategorias][resultado]', res.resultado);
        setCategorias(res.resultado);
    }

    /**
     * 
     * Variables globales
     * 
     */

    const grupoCategorias = [];


    /**
     * 
     * Array para obtener los banners secundarios y acomodarlos de 2 en 2
     * 
     */
    for (let i = 0; i < categorias?.subcategoriasEspecificas?.length; i += 2) {
        const grupo = (
            <Grid container spacing={2} key={i}>
                <Grid item xs={12} sm={6}>
                    <CategoriasComponent urlImagen={categorias.subcategoriasEspecificas[i].urlImagen} posicion={i} criteriosConsulta={categorias.subcategoriasEspecificas[i].criteriosConsulta} idCategoria={sharedData.id ?? 1} nombre={categorias.subcategoriasEspecificas[i].nombre} />
                </Grid>
                {i + 1 < categorias.subcategoriasEspecificas.length && (
                    <Grid item xs={12} sm={6}>
                        <CategoriasComponent urlImagen={categorias.subcategoriasEspecificas[i + 1].urlImagen} posicion={i + 1} criteriosConsulta={categorias.subcategoriasEspecificas[i + 1].criteriosConsulta} idCategoria={sharedData.id ?? 1} nombre={categorias.subcategoriasEspecificas[i + 1].nombre} />
                    </Grid>
                )}
            </Grid>
        );

        grupoCategorias.push(grupo);
    }

    /**
     * 
     * Retorna la vista de categorias
     * 
     */
    return (
        <Grid item xs={6}>
            {loader ? (
                <Loader />
            ) : (<Paper elevation={3} style={{ padding: 20 }}>
                <Typography variant="h5" align="center" gutterBottom>
                    Categorias {sharedData.id}
                </Typography>
                {grupoCategorias}
                <br />
            </Paper>)}
        </Grid>
    );
}

export default ColumnaVistaCategorias;