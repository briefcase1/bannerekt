/**
 * 
 * 
 * Importaciones
 * 
 */
import { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Grid, TextField } from '@mui/material';
import getCategoriasLista from '../api/getCategoriasLista';
import useDataCat from '../data/dataListaCat';
import useDataStoreFCatL from '../data/dataListaCatF';
import Button from '@mui/material/Button';
import Loader from './Loader';

/**
 * 
 * Funcion para regresar la vista de la lista de categorias 
 * 
 * 
 */
function ColumnaVistaCatL() {

    /**
     * 
     * 
     * constantes
     * 
     */
    const [categorias, setCategorias] = useState();
    const [searchTerm, setSearchTerm] = useState('');
    const [loader, setLoader] = useState(true);

    /**
     * 
     * Funcion para traer la informacion que ha cambiado del formulario
     * 
     */
    const sharedData = useDataCat((state) => state.sharedData);
    console.log('[columnaVistaCategoriasLista][sharedData]', sharedData);
    const setSharedData = useDataStoreFCatL((state) => state.setSharedData);


    /**
     * 
     * 
     * UseEffect para actualizar la info al inciar o cuando cambie el dato
     * 
     */
    useEffect(() => {
        if (sharedData === '') {
            datos();
        } else {
            datos();
        }
    }, [sharedData]);


    /**
     * 
     * Funcion para obtener los datos de la lista de categorias  Lista
     * 
     */
    const datos = async () => {
        console.log('[columnaVistaCategoriasLista][datos][inciando] . .');
        const res = await getCategoriasLista();
        console.log('[columnaVistaCategoriasLista][datos][resultado]', res.resultado);
        setCategorias(res.resultado);
        setLoader(false);
    }


    /**
     * 
     * 
     * Funcion para importar los datos de la tabla al formulario
     *  
     */
    const handleImport = (rowData) => {
        console.log('[columnaVistaCategoriasLista][handleImport][data]', rowData);
        const newCriteriosConsulta = {
            id: rowData.id,
            nombre: rowData.nombre,
            url: rowData.url,
        };
        setSharedData(newCriteriosConsulta);
        console.log('[columnaVistaCategoriasLista][handleImport][criterios]', newCriteriosConsulta);
    };


    /**
     * 
     * Funcion para filtrar los datos de la yabla
     * 
     */
    const filteredColecciones = categorias?.filter((item) =>
        item.id.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.url.toLowerCase().includes(searchTerm.toLowerCase())
    );


    return (
        <Grid item xs={6}>
            <TextField
                label="Buscar"
                variant="outlined"
                fullWidth
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                style={{ marginBottom: '20px', backgroundColor: '#f5f5f5' }}
            />
            {loader ? (
                <Loader />
            ) : (
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Nombre</TableCell>
                                <TableCell>URL</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {filteredColecciones?.map((item) => (
                                <TableRow key={item?.id}>
                                    <TableCell>{item?.id}</TableCell>
                                    <TableCell>{item?.nombre}</TableCell>
                                    <TableCell>{item?.url ?? ''}</TableCell>
                                    <TableCell>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={() => handleImport(item)}
                                        >
                                            Importar
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
        </Grid>
    );
}

export default ColumnaVistaCatL;
