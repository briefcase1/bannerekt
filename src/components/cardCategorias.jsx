/* eslint-disable react/prop-types */
/**
 * 
 * Impotacion
 * 
 */
import { CardMedia, Card, CardContent, Button } from '@mui/material';
import useDataStoreFC from '../data/dataCategoriaF';

/**
 * 
 * Funcion para visualizar las card de categorias de tipo descubre
 * 
 */
const CategoriasComponent = ({ criteriosConsulta, urlImagen, posicion, idCategoria, nombre }) => {

    /**
     * Funcion para actualizar la informacion de useDataStoreFC Formulario de categorias
     * 
     */
    const setSharedData = useDataStoreFC((state) => state.setSharedData);

    /**
     * 
     * Manjeador al darle click a la imagen
     * 
     */
    const handleImageClick = (criteriosConsulta, posicion) => {

        const newCriteriosConsulta = {
            ...criteriosConsulta, // criterios de consulta de la cetgoria
            posicion: posicion, // la posicion de los cards
            urlImagen: urlImagen, // la imagen de la categoria
            idCategoria, //  el id de categoria
            titulo: nombre // el nombre de la configuracion
        };

        console.log('[CategoriasComponent][handleImageClick][criterops]', newCriteriosConsulta);
        setSharedData(newCriteriosConsulta);
    };

    return (
        <div>
            <center>
                <Card>
                    <CardMedia
                        component="img"
                        alt="Imagen"
                        image={urlImagen}
                        onClick={() => handleImageClick(criteriosConsulta)}
                        style={{ width: '80%', height: '50%' }}
                    />
                    <CardContent>
                        {nombre} <br />
                        <Button variant="contained" color="success" onClick={() => handleImageClick(criteriosConsulta, posicion)}>
                            Modificar
                        </Button>
                    </CardContent>
                </Card>
            </center>
        </div>
    );
};

/**
 * 
 * Exportaciones
 * 
 */
export default CategoriasComponent;