/**
 * 
 * 
 * Importaciones
 * 
 */
import { useEffect, useState } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Grid, TextField } from '@mui/material';
import getColecciones from '../api/getColecciones';
import useDataColecciones from '../data/dataColecciones';
import useDataStoreFCol from '../data/dataColeccionesF';
import Button from '@mui/material/Button';
import Loader from './Loader';

/**
 * 
 * Funcion para regresar la vista de la tabla  de colecciones 
 * 
 */
function ColumnaVistaColecciones() {

    /**
     * 
     * Constantes de datos
     * 
     */
    const [coleciones, setColecciones] = useState();
    const [searchTerm, setSearchTerm] = useState('');
    const [loader, setLoader] = useState(true);

    const sharedData = useDataColecciones((state) => state.sharedData);
    console.log('[columnaVistaColecciones][sharedData]', sharedData);

    /**
     * 
     * Funcion de los datos del formualrio
     * 
     */
    const setSharedData = useDataStoreFCol((state) => state.setSharedData);


    /**
     * 
     * 
     * useEffect para ejecutar codigo al inicio o si cambia de valor las variables
     * indicadas
     * 
     */
    useEffect(() => {
        if (sharedData === '') {
            datos();
        } else {
            datos();
        }
    }, [sharedData]);

    /**
     * 
     * Funcion para obtener los datos
     * 
     */
    const datos = async () => {
        console.log('[columnaVistaColecciones][datos][inciando] . .');
        const res = await getColecciones();
        console.log('[columnaVistaColecciones][datos][resultado]', res.resultado);
        setColecciones(res.resultado);
        setLoader(false);
    }


    /**
     * 
     * Funcion para importar los datos de la tabla a al formulario
     *  
     */
    const handleImport = (rowData) => {
        console.log('[columnaVistaColecciones][handleImport][rowData]', rowData);
        const newCriteriosConsulta = {
            idColeccion: rowData.id,
            idSubColeccion: rowData.subcolecciones[0]?.id?.split('/')[1],
            nombre: rowData.nombre,
            url: rowData.url,
        };
        console.log('[columnaVistaColecciones][handleImport][rowData]', newCriteriosConsulta);
        setSharedData(newCriteriosConsulta);
    };


    /**
     * 
     * Funcion para filtrar entre las colecciones el campo buscado
     * 
     */
    const filteredColecciones = coleciones?.colecciones?.filter((item) =>
        item.subcolecciones[0].id.toLowerCase().includes(searchTerm.toLowerCase()) ||
        item.url.toLowerCase().includes(searchTerm.toLowerCase())
    );


    return (
        <Grid item xs={6}>
            <TextField
                label="Buscar"
                variant="outlined"
                fullWidth
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                style={{ marginBottom: '20px', backgroundColor: '#f5f5f5' }}
            />
            {loader ? (
                <Loader />
            ) : (
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Nombre</TableCell>
                                <TableCell>SubColeccion</TableCell>
                                <TableCell>URL</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {filteredColecciones?.map((item) => (
                                <TableRow key={item?.id}>
                                    <TableCell>{item?.id}</TableCell>
                                    <TableCell>{item?.nombre}</TableCell>
                                    <TableCell>{item?.subcolecciones[0]?.id?.split('/')[1]}</TableCell>
                                    <TableCell>{item?.url ?? ''}</TableCell>
                                    <TableCell>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={() => handleImport(item)}
                                        >
                                            Importar
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
        </Grid>
    );
}

export default ColumnaVistaColecciones;
