/**
 * 
 * Importaciones
 * 
 */
import React, { useState } from "react";
import '../App.css';
import { Grid, Paper, Typography } from '@mui/material';
import BannerComponent from './cardBanners';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import LoMasHotComponent from './cardLoMasHot';
import PopularComponent from './cardPopular';
import getBanners from '../api/getBanners';
import useDataBanners from '../data/dataBanners';
import Loader from './Loader';

/**
 * 
 * Funcion que regresa la vista con las secciones
 * 
 */
const ColumnaVista = () => {

    /**
     * 
     * Constante para guardar la informacion de los banners  
     */
    const [banners, setBanners] = useState([]);
    const [loader, setLoader] = useState(true);

    /**
     * 
     * Funcion para compartir la informacion de los banners desde el formulario
     */
    const sharedData = useDataBanners((state) => state.sharedData);

    /**
     * 
     * Si no hay datos compartidos consultamos la informacion, en caso contrario
     * agregamos los datos de los banners
     * 
     */
    React.useEffect(() => {
        if (sharedData === '') {
            datos();
        } else {

            setBanners(sharedData);
        }
    }, [sharedData])


    //Función para obtener la información de los datos
    const datos = async () => {
        console.log('[ColumnaVista][datos]', datos);
        const res = await getBanners();
        console.log('[ColumnaVista][res]', res.resultado);
        setBanners(res.resultado);
        setLoader(false);
    }

    /**
     * 
     * Variables globales para almacenar la infomacion de lo Mas Hot y populares
     * 
     */
    const gruposDeLoMasHot = [];
    const grupoPopulares = [];


    /**
     * 
     * Array para obtener los banners secundarios y acomodarlos de 2 en 2
     * 
     */
    for (let i = 0; i < banners?.anuncios?.secundarios?.length; i += 2) {
        const grupo = (
            <Grid container spacing={2} key={i}>
                <Grid item xs={12} sm={6}>
                    <LoMasHotComponent urlImagen={banners.anuncios.secundarios[i].urlImagen} posicion={i} criteriosConsulta={banners.anuncios.secundarios[i].criteriosConsulta} />
                </Grid>
                {i + 1 < banners.anuncios.secundarios.length && (
                    <Grid item xs={12} sm={6}>
                        <LoMasHotComponent urlImagen={banners.anuncios.secundarios[i + 1].urlImagen} posicion={i + 1} criteriosConsulta={banners.anuncios.secundarios[i + 1].criteriosConsulta} />
                    </Grid>
                )}
            </Grid>
        );

        gruposDeLoMasHot.push(grupo);
    }


    /**
     * 
     * Ciclo para sacar los banners de categorias populares 
     * 
     */
    for (let i = 0; i < banners?.anuncios?.categoriasPopulares?.length; i += 2) {
        const grupo = (
            <Grid container spacing={2} key={i}>
                <Grid item xs={12} sm={6}>
                    <PopularComponent urlImagen={banners.anuncios.categoriasPopulares[i].urlImagen} posicion={i} criteriosConsulta={banners.anuncios.categoriasPopulares[i].criteriosConsulta} />
                </Grid>
                {i + 1 < banners.anuncios.categoriasPopulares.length && (
                    <Grid item xs={12} sm={6}>
                        <PopularComponent urlImagen={banners.anuncios.categoriasPopulares[i + 1].urlImagen} posicion={i + 1} criteriosConsulta={banners.anuncios.categoriasPopulares[i + 1].criteriosConsulta} />
                    </Grid>
                )}
            </Grid>
        );

        grupoPopulares.push(grupo);
    }


    //Retorna la vista de Home con las secciones de principales ,secundarios y populares
    return (

        <Grid item xs={6}>
            {loader ? (
                <Loader />
            ) : (
                <Paper elevation={3} style={{ padding: 20 }}>
                    <Typography variant="h5" align="center" gutterBottom>
                        Banners Principales
                    </Typography>
                    <Carousel
                        showArrows={true}
                        showThumbs={false}
                        infiniteLoop={true}
                        autoPlay={true}
                        interval={30000}
                        emulateTouch={true}
                    >

                        {banners?.anuncios?.principales?.map((banner, index) => (
                            <BannerComponent key={index} urlImagen={banner.urlImagen} criteriosConsulta={banner.criteriosConsulta} posicion={index} />
                        ))}

                    </Carousel>
                    <br />
                    <Typography variant="h5" align="center" gutterBottom>
                        Lo mas Hot
                    </Typography>

                    {gruposDeLoMasHot}

                    <br />
                    <Typography variant="h5" align="center" gutterBottom>
                        Categorias Populares
                    </Typography>

                    {grupoPopulares}

                </Paper>)}
        </Grid>
    );
}

export default ColumnaVista;