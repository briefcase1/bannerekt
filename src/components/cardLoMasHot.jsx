/* eslint-disable react/prop-types */
/**
 * 
 * Impotacion
 * 
 */
import { CardMedia, Card, CardContent, Button } from '@mui/material';
import useDataStore from '../data/data';

/** 
 * 
 * Funcion para visualizar el componente de lo mas hot
 * 
 */
const LoMasHotComponent = ({ criteriosConsulta, urlImagen, posicion }) => {

    /**
    * Funcion para actualizar los datos de useDataStore(Datos del formulario de banners Home)
    */
    const setSharedData = useDataStore((state) => state.setSharedData);

    /**
     * 
     * Manjeador al darle click a la imagen
     * 
     */
    const handleImageClick = (criteriosConsulta, posicion) => {

        const newCriteriosConsulta = {
            ...criteriosConsulta,
            posicion: posicion,
            urlImagen: urlImagen,
            seccionPos: '2',
        };
        console.log('[LoMasHotComponent][handleImageClick][criterops]', newCriteriosConsulta);
        setSharedData(newCriteriosConsulta);
    };

    return (
        <div>
            <center>
                <Card>
                    <CardMedia
                        component="img"
                        alt="Imagen"
                        image={urlImagen}
                        onClick={() => handleImageClick(criteriosConsulta)}
                        style={{ width: '50%', height: '50%' }}
                    />
                    <CardContent>
                        <Button variant="contained" color="success" onClick={() => handleImageClick(criteriosConsulta, posicion)}>
                            Modificar
                        </Button>
                    </CardContent>
                </Card>
            </center>
        </div>
    );
};

/**
 * 
 * Exportaciones
 * 
 */
export default LoMasHotComponent;