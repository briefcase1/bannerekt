/* eslint-disable no-case-declarations */
/**
 * 
 * 
 * Importaciones
 * 
 */
import React, { useState, useEffect } from "react";
import '../App.css';
import {
    TextField, Button, Grid, Paper, FormControl, MenuItem, InputLabel, Select
    , Typography
} from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import formulario from '../store/formularioCategoias';
import useDataStoreFC from '../data/dataCategoriaF';
import useDataCategorias from '../data/dataCategoria';
import categoriasJson from '../resources/categoriasApp.json';
import getCategorias from '../api/getCategorias';
import putCategorias from '../api/putCategorias';
import categoriasData from '../resources/categorias.json';
import getCategoria from '../api/getCategoria';
import getColecciones from '../api/getColecciones';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CircularProgress from '@mui/material/CircularProgress';

const categoriasDepartamentos = categoriasJson;

/**
 * Funcio para retornar el formulario
 * de la vista de categorias
 */
const ColumnaFormularioCat = () => {

    /**
     * 
     * 
     * Constantes
     * 
     */
    //const [categoriasC, setCategoriasC] = useState([]);
    const [listaCategorias, setListaCategorias] = useState([]);
    const [idCategoriaB, setIdCategoriaB] = useState(1);
    const [bandAct, setBandAct] = useState(true);
    const [categoriasSelect, setCategoriasSelect] = useState([]);
    const [coleccionesSelect, setColeccionesSelect] = useState([]);
    const [loader, setLoader] = useState(false);
    const [loaderMod, setLoaderMod] = useState(false);
    const [loaderDel, setLoaderDel] = useState(false);


    /**
     * 
     * Uso del useEffect para ejecutar codigo al inciar o cuando
     * una de las variables indicadas sufran algun cambio
     * 
     */
    React.useEffect(() => {
        datos(idCategoriaB);
        if (bandAct) {
            datosCategorias();
            setBandAct(false);
        }
    }, [idCategoriaB, bandAct])


    /**
     * 
     * Constantes para guardar la informacion de las categorias y ser compartida con el 
     * componente vista para visualizar los cambios
     */
    const setSharedData = useDataCategorias((state) => state.setSharedData);
    const sharedDataCategorias = useDataCategorias((state) => state.sharedData);

    //Función para obtener la información de los datos para los inputs
    const datos = async (id) => {
        console.log('[columnaFormularioCategorias][datos]. .',);
        const res = await getCategoria(id ?? 1);
        console.log('[columnaFormularioCategorias][resultado]', res.resultado);
        setSharedData(res.resultado);
        /////////////////
        const resCat = await getCategorias();
        console.log('[ColumnaFormularioCategorias][resultado][categorias]', resCat);
        setCategoriasSelect(resCat.resultado);
        //////////////
        let resColec = await getColecciones();
        console.log('[ColumnaFormularioCategorias][respuesta][colecciones]', resColec);
        resColec = resColec.resultado.colecciones.map(item => {
            return {
                "label": `${item.subcolecciones[0].id} [${item.nombre} - ${item.url}]`,
                "value": item.subcolecciones[0].id
            }
        });
        setColeccionesSelect(resColec);
    }

    //Funcion para obtener los datos de la lista de categorias
    //si si cambia se debe actualizar la vista de las imagenes de las categorias
    const datosCategorias = async () => {
        if (listaCategorias.length <= 0) {
            console.log('[columnaFormularioCategorias][datosCategorias] . .');
            const res = await getCategorias();
            console.log('[columnaFormularioCategorias][resultado]', res);
            setListaCategorias(res.resultado);
        }
    }


    /**
     * 
     * Destructuracion de datos de formulario
     * 
     */
    const { filtro, ordenamiento, categorias
        , caracteristicas, seccion, tipoBanner, posicion, urlImagen,
        seccionId, nombreSeccion, idCategoria, titulo, ids,
        colecciones, categoriasCombinadas } = formulario(state => state);



    /**
     * 
     * Banderas para mostrar distintas opciones del formulario
     * 
     */
    const [showIdSection, setShowIdSection] = useState(false);
    const [showNombreSetccion, setShowNombreSetccion] = useState(false);
    const [showFiltro, setShowFiltro] = useState(true);
    const [showOrdenamiento, setShowOrdenamiento] = useState(true);




    /**
     * 
     * Constante de los datos del formulario
     * 
     */
    const [formData, setFormData] = useState({
        filtro: filtro, //producto.filtro
        ordenamiento: ordenamiento, //producto.ordenamiento
        categorias: categorias, //No existe
        idCategoria: idCategoria, //id de la coleccion de categoria
        caracteristicas: caracteristicas, // producto.caracteristicas ó seccion.caracteristicas
        seccion: seccion, //Tipo de seccion productos o seccion
        tipoBanner: tipoBanner, // Populares, Hot, Principales
        posicion: posicion, //posicion del banners
        urlImagen: urlImagen, // imagen del banner
        seccionId: seccionId, // seccion.id
        nombreSeccion: nombreSeccion, // seccion.nombre
        titulo: titulo,// titulo de la card
        ids: ids, // id de las categorias
        colecciones: colecciones, //conjunto de colecciones de PLP
        categoriasCombinadas: categoriasCombinadas // conjunto de categorias combinas
    });

    /**
     * 
     * Asignacion de la data compartida entre las categorias y formularios
     * 
     */
    const sharedData = useDataStoreFC((state) => state.sharedData);
    console.log('[columnaFormularioCategorias][sharedData]', sharedData);


    /**
     * 
     * Use Effect para actualizar informacion en cada refresh
     */
    useEffect(() => {

        if (sharedDataCategorias === '') {
            datos();
        } else {
            //setCategoriasC(sharedDataCategorias);
        }

        setFormData({
            filtro: sharedData?.producto?.filtro ?? '',
            ordenamiento: sharedData?.producto?.ordenamiento ?? '',
            categorias: null,
            idCategoria: sharedData?.idCategoria ?? '',
            caracteristicas: sharedData?.id === 1 ? sharedData?.producto?.caracteristicas : sharedData?.seccion?.caracteristicas ?? '',
            seccion: sharedData?.id ?? '1',
            tipoBanner: sharedData?.seccionPos ?? '1',
            posicion: sharedData?.posicion ?? 0,
            urlImagen: sharedData?.urlImagen ?? '',
            nombreSeccion: sharedData?.seccion?.nombre ?? '',
            seccionId: sharedData?.seccion?.id ?? '1',
            titulo: sharedData?.titulo ?? '',
            ids: null,
            colecciones: null,
            categoriasCombinadas: null
        })

        console.log('[columnaFormularioCategorias][seccion]', sharedData.id === 2)
        if (sharedData.id != 2) {
            setShowIdSection(false);
            setShowNombreSetccion(false);
            setShowFiltro(true);
            setShowOrdenamiento(true);
        } else {
            setShowIdSection(true);
            setShowNombreSetccion(true);
            setShowFiltro(false);
            setShowOrdenamiento(false);
        }
    }, [sharedData, sharedDataCategorias]);


    /**
     * 
     * Funcion para actualizar los datos del formualrio
     * 
     */
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };


    /**
     *
     * Funcion para modificar la posicion de los banners
     *
     */
    const handlePosicionChange = (event) => {
        const { name, value } = event.target;
        console.log('[columnaFormulario][handlePosicionChange][value]', value)
        console.log('[columnaFormulario][handlePosicionChange][name]', name)
        setFormData(prevFormData => ({
            ...prevFormData,
            [name]: value
        }));

    }

    /**
     * 
     * Funcion que dependiendo del tipo de seccion (producto ó  secciones) cambiara las opciones
     * 
     */
    const handleTipoChange = (event, newValue) => {
        setFormData({
            ...formData,
            seccion: newValue.props.value
        });

        if (formData.seccion == '2') {
            setShowIdSection(false);
            setShowNombreSetccion(false);
            setShowFiltro(true);
            setShowOrdenamiento(true);
        } else {
            setShowIdSection(true);
            setShowNombreSetccion(true);
            setShowFiltro(false);
            setShowOrdenamiento(false);
        }

    };


    /**
     * 
     * Funcion para cambiar el tipo de seccion
     * 
     */
    const handleTipoIdSeccion = (event, newValue) => {
        console.log('[columnaFormulariaCategorias][handleTipoIdSeccion][newValue]', newValue);
        setFormData({
            ...formData,
            seccionId: newValue.props.value
        })
    }

    /**
    * 
    * Funcion para cambiar el valor de las colecciones
    * 
    */
    const handleColeccioneChange = (event, newValue) => {
        console.log('[ColumnaFormularioCategorias][handleCategoriasChange][newValue]', newValue);
        setFormData({
            ...formData,
            colecciones: newValue
        });
    };

    /**
    * 
    * Funcion seleccionar el valor de una categoria combina
    */
    const handleCategoriasCombinadasChange = (event, newValue) => {
        console.log('[ColumnaFormularioCategorias][handleCategoriasCombinadasChange][newValue]', newValue);
        setFormData({
            ...formData,
            categoriasCombinadas: newValue
        });
    };


    /**
     * 
     * Funcion para cambiar el valor de categorias
     * 
     */
    const handleCategoriasChange = (event, newValue) => {
        console.log('[columnaFormularioCategorias][handleCategoriasChange][newValue]', newValue)
        setFormData({
            ...formData,
            categorias: newValue
        });
    };



    /**
     * 
     * Funcion para cambiar el valor de categorias
     * 
     */
    const handleCategoriasIdChange = (event, newValue) => {
        console.log('[columnaFormularioCategorias][handleCategoriasIdChange][newValue]', newValue)
        setFormData({
            ...formData,
            ids: newValue
        });
        setIdCategoriaB(newValue.value);
    };

    /**
     * 
     * Funcion para guardar la informacion
     * 
     */
    const handleSubmit = async (event) => {
        try {
            setLoader(true);
            event.preventDefault();
            console.log('[columnaFormulario][handleSubmit][formData]', formData);
            let body = {
                criteriosConsulta: {
                    id: '',
                    nombre: '',
                    producto: {
                        filtro: '',
                        ordenamiento: '',
                        caracteristicas: '',
                    },
                    seccion: {
                        id: '',
                        nombre: '',
                        caracteristicas: '',
                    },
                },
                urlImagen: '',
            };

            const band = formData?.categorias?.value ? true : false;

            switch (String(formData.seccion)) {
                case '1':
                    getBodyProductos(formData, body, band);
                    break;
                case '2':
                    getBodySecciones(formData, body, band);
                    break;
            }

            console.log('[columnaFormulario][handleSubmit][body]', body);

            formData.idCategoria = !formData.idCategoria ? formData?.ids?.value : formData.idCategoria;
            await putCategorias(formData.posicion, "1", formData.idCategoria, body);
            await datos(formData.idCategoria);
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoader(false);
        } catch (error) {
            console.error('[columnaFormulario][handleSubmit][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false)
        }
    };


    /**
     * 
     *Funcion para eliminar categorias
     *  
     */
    const handleEliminar = async (event) => {
        try {
            setLoaderDel(true);
            event.preventDefault();
            console.log('[columnaFormulario][handleEliminar][formData]', formData);
            await putCategorias(formData.posicion, "1", formData.categorias, {});
            formData.idCategoria = !formData.idCategoria ? formData?.ids?.value : formData.idCategoria;
            await putCategorias(formData.posicion, "3", formData.idCategoria, {});
            await datos(formData.idCategoria);
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoaderDel(false);
        } catch (error) {
            console.error('[columnaFormulario][handleEliminar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoaderDel(false);
        }
    }


    /**
     *
     *
     * Funcion para modificar la informacion de las cards
     *
     */
    const handleModificar = async (event) => {
        try {
            event.preventDefault();
            console.log('[columnaFormulario][handleModificar][formData]', formData);
            setLoaderMod(true);
            let body = {
                criteriosConsulta: {
                    id: '',
                    nombre: '',
                    producto: {
                        filtro: '',
                        ordenamiento: '',
                        caracteristicas: '',
                    },
                    seccion: {
                        id: '',
                        nombre: '',
                        caracteristicas: '',
                    },
                },
                urlImagen: '',
            };

            const band = formData?.categorias?.value ? true : false;

            switch (String(formData.seccion)) {
                case '1':
                    getBodyProductos(formData, body, band);
                    break;
                case '2':
                    getBodySecciones(formData, body, band);
                    break;
            }

            console.log('[columnaFormulario][handleModificar][body]', body);
            formData.idCategoria = !formData.idCategoria ? formData?.ids?.value : formData.idCategoria;
            await putCategorias(formData.posicion, "2", formData.idCategoria, body);
            await datos(formData.idCategoria);
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoaderMod(false);
        } catch (error) {
            console.error('[columnaFormulario][handleModificar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    }



    function getBodyProductos(formData, body, crear) {
        if (crear) {
            let base = 'category-';

            let baseCat = ''

            const valorCategorias = formData.categorias.value.split('---');
            const rutas = valorCategorias[1].split('/');


            for (let x = 0; x < rutas.length; x++) {
                baseCat = `${baseCat}${base}${x + 1}/${rutas[x]}/`;
            }

            //Armado
            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            body['criteriosConsulta']['id'] = 1;
            body['criteriosConsulta']['nombre'] = 'productos';
            body['criteriosConsulta']['producto']['caracteristicas'] = baseCat;
            body['criteriosConsulta']['producto']['ordenamiento'] = '';
            body['criteriosConsulta']['producto']['filtro'] = '';
            body['criteriosConsulta']['seccion']['id'] = '0';
            body['criteriosConsulta']['seccion']['nombre'] = 'NA';
            body['criteriosConsulta']['seccion']['caracteristicas'] = 'NA';
            body['urlImagen'] = formData.urlImagen;
        } else {
            //Armado
            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            body['criteriosConsulta']['id'] = 1;
            body['criteriosConsulta']['nombre'] = 'productos';
            body['criteriosConsulta']['producto']['caracteristicas'] = formData.caracteristicas ?? '';
            body['criteriosConsulta']['producto']['ordenamiento'] = formData.ordenamiento ?? '';
            body['criteriosConsulta']['producto']['filtro'] = formData.filtro ?? '';
            body['criteriosConsulta']['seccion']['id'] = '0';
            body['criteriosConsulta']['seccion']['nombre'] = 'NA';
            body['criteriosConsulta']['seccion']['caracteristicas'] = 'NA';
            body['urlImagen'] = formData.urlImagen;
        }
    }


    function getBodySecciones(formData, body, crear) {
        if (crear) {
            let baseCat = '';
            let baseNombre = ''

            const valorCategorias = formData.categorias.value.split('---');
            console.log(valorCategorias[1]);
            const rutas = valorCategorias[1].split('/');

            for (let x = 0; x < rutas.length; x++) {
                if (x === 0) {
                    let catgeroriaAux = categoriasDepartamentos.categorias.find((element) => quitarAcentos(element.nombre.toLowerCase()) === rutas[x]);
                    baseNombre = baseNombre + catgeroriaAux['nombre'] + '/';
                    baseCat = baseCat + catgeroriaAux['id'] + '/';
                } else {
                    let catgeroriaAux = categoriasDepartamentos.categorias.find((element) => quitarAcentos(element.nombre.toLowerCase()) === rutas[x] && element.id === valorCategorias[2]);
                    baseNombre = baseNombre + catgeroriaAux['nombre'] + '/';
                    baseCat = baseCat + catgeroriaAux['id'] + '/';
                }
            }

            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = formData.seccionId;
            body['criteriosConsulta']['seccion']['nombre'] = baseNombre.slice(0, baseNombre.length - 1);
            body['criteriosConsulta']['seccion']['caracteristicas'] = baseCat.slice(0, baseCat.length - 1);
            body['urlImagen'] = formData.urlImagen;
        } else if (formData.seccionId == 1) {

            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = 'colecciones';
            body['criteriosConsulta']['seccion']['caracteristicas'] = formData?.colecciones?.value ?? formData.caracteristicas;
            body['urlImagen'] = formData.urlImagen
        } else if (formData.seccionId == 3) {

            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            const category = formData?.categoriasCombinadas?.label?.split('--') ?? [];
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = Number(formData.seccionId);
            body['criteriosConsulta']['seccion']['nombre'] = !category[1] ? formData.nombreSeccion : category[1];
            body['criteriosConsulta']['seccion']['caracteristicas'] = !category[0] ? formData.caracteristicas : category[0];
            body['urlImagen'] = formData.urlImagen;
        }
        else {
            body['id'] = formData.posicion;
            body['idNivel'] = 1;
            body['nombre'] = formData.titulo;
            body['criteriosConsulta']['id'] = 2;
            body['criteriosConsulta']['nombre'] = 'secciones';
            body['criteriosConsulta']['producto']['caracteristicas'] = 'NA';
            body['criteriosConsulta']['producto']['ordenamiento'] = 'NA';
            body['criteriosConsulta']['producto']['filtro'] = 'NA';
            body['criteriosConsulta']['seccion']['id'] = formData.seccionId;
            body['criteriosConsulta']['seccion']['nombre'] = formData.nombreSeccion;
            body['criteriosConsulta']['seccion']['caracteristicas'] = formData.caracteristicas;
            body['urlImagen'] = formData.urlImagen;
        }
    }


    function quitarAcentos(texto) {
        return texto.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }

    return (
        <Grid item xs={6}>
            <ToastContainer />
            <Paper elevation={3} style={{ padding: 20 }}>
                <Typography variant="h5" align="center" gutterBottom>
                    Categorias
                </Typography>
                <form onSubmit={handleSubmit}>
                    <TextField
                        label="Titulo"
                        name="titulo"
                        fullWidth
                        value={formData.titulo}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    <FormControl fullWidth>
                        <InputLabel id="tipo-label">Tipo</InputLabel>
                        <Select
                            labelId="tipo-label"
                            id="tipo-select"
                            value={formData.seccion}
                            label="Tipo"
                            onChange={handleTipoChange}
                        >
                            <MenuItem value="1">Productos</MenuItem>
                            <MenuItem value="2">Seccion</MenuItem>
                        </Select>
                    </FormControl>
                    <Autocomplete
                        id="categorias"
                        options={categoriasData}
                        getOptionLabel={(option) => option.label}
                        value={formData.categorias}
                        onChange={handleCategoriasChange}
                        renderInput={(params) => <TextField {...params} label="Categorias" margin="normal" />}
                    />
                    <Autocomplete
                        id="listaCategorias"
                        options={listaCategorias}
                        getOptionLabel={(option) => option.label}
                        value={formData.ids}
                        onChange={handleCategoriasIdChange}
                        renderInput={(params) => <TextField {...params} label="Listado Categorias" margin="normal" />}
                    />
                    <TextField
                        label="Posición"
                        name="posicion"
                        type="number"
                        fullWidth
                        value={formData.posicion}
                        onChange={handlePosicionChange}
                        margin="normal"
                    />

                    {showIdSection &&
                        (<FormControl fullWidth>
                            <InputLabel id="tipo-id-seccion">Seccion ID</InputLabel>
                            <Select
                                labelId="tipo-id-seccion"
                                id="tipo-id-seccion"
                                value={formData.seccionId}
                                label="ID de seccion"
                                onChange={handleTipoIdSeccion}
                            >
                                <MenuItem value="0"></MenuItem>
                                <MenuItem value="1">PLP Colecciones</MenuItem>
                                <MenuItem value="2">Categorias</MenuItem>
                                <MenuItem value="3">Categorias Combinadas</MenuItem>
                            </Select>
                        </FormControl>)}

                    {Number(formData.seccionId) === 1 && Number(formData.seccion) === 2 && (<Autocomplete
                        id="colecciones"
                        options={coleccionesSelect}
                        getOptionLabel={(option) => option.label}
                        value={formData.colecciones}
                        onChange={handleColeccioneChange}
                        renderInput={(params) => <TextField {...params} label="Colecciones" margin="normal" />}
                    />)}

                    {Number(formData.seccionId) === 3 && Number(formData.seccion) === 2 && (<Autocomplete
                        id="categoriasCombinadas"
                        options={categoriasSelect}
                        getOptionLabel={(option) => option?.label}
                        value={formData.categoriasCombinadas}
                        onChange={handleCategoriasCombinadasChange}
                        renderInput={(params) => <TextField {...params} label="Categorias Combinadas" margin="normal" />}
                    />)}

                    {showNombreSetccion && (<TextField
                        label="Nombre de seccion"
                        name="nombreSeccion"
                        fullWidth
                        value={formData.nombreSeccion}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    )}

                    {showFiltro && (
                        <TextField
                            label="Filtro"
                            name="filtro"
                            fullWidth
                            value={formData.filtro}
                            onChange={handleInputChange}
                            margin="normal"
                        />

                    )}

                    {showOrdenamiento && (<TextField
                        label="Ordenamiento"
                        name="ordenamiento"
                        fullWidth
                        value={formData.ordenamiento}
                        onChange={handleInputChange}
                        margin="normal"
                    />)}

                    <TextField
                        label="Caracteristicas"
                        name="caracteristicas"
                        fullWidth
                        value={formData.caracteristicas}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    <TextField
                        label="Imagen"
                        name="urlImagen"
                        fullWidth
                        value={formData.urlImagen}
                        onChange={handleInputChange}
                        margin="normal"
                    />
                    <Button fullWidth type="submit" variant="contained" color="primary">
                        {loader ? <CircularProgress size={24} color="inherit" /> : 'Crear'}
                    </Button>
                </form>
                <br />
                <Button fullWidth onClick={handleModificar} variant="contained" color="secondary" >
                    {loaderMod ? <CircularProgress size={24} color="inherit" /> : 'Modificar'}
                </Button>
                <br />
                <br />
                <Button fullWidth onClick={handleEliminar} variant="contained" color="error">
                    {loaderDel ? <CircularProgress size={24} color="inherit" /> : 'Eliminar'}
                </Button>
            </Paper>
        </Grid>
    );
}

export default ColumnaFormularioCat;