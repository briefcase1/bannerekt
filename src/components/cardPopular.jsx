/* eslint-disable react/prop-types */
/**
 * 
 * Importaciones
 * 
 */
import { CardMedia, Card, CardContent, Button } from '@mui/material';
import useDataStore from '../data/data';


/**
 * 
 * Funcion para regresar el diseño de la card de populares
 * 
 */
const PopularComponent = ({ criteriosConsulta, urlImagen, posicion }) => {

    /**
    * Funcion para actualizar los datos de useDataStore(Datos del formulario de banners Home)
    */
    const setSharedData = useDataStore((state) => state.setSharedData);

    /**
     * Manejador al darle clic a la imagen o el botón
     */
    const handleImageClick = () => {
        const newCriteriosConsulta = {
            ...criteriosConsulta,
            posicion: posicion,
            urlImagen: urlImagen,
            seccionPos: '3',
        };

        console.log('[PopularComponent][handleImageClick][criterops]', newCriteriosConsulta);

        setSharedData(newCriteriosConsulta);
    };

    return (
        <div>
            <center>
                <Card>
                    <CardMedia
                        component="img"
                        alt="Imagen"
                        image={urlImagen}
                        onClick={handleImageClick}
                        style={{ width: '50%', height: '50%', cursor: 'pointer' }}
                    />
                    <CardContent>
                        <Button variant="contained" color="success" onClick={handleImageClick}>
                            Modificar
                        </Button>
                    </CardContent>
                </Card>
            </center>
        </div>
    );
};

/**
 * 
 * Exportaciones
 * 
 */
export default PopularComponent;
