/* eslint-disable no-case-declarations */

/**
 * 
 * 
 * Importaciones
 * 
 */
import React, { useState } from "react";
import { useEffect } from 'react';
import '../App.css';
import {
    TextField, Button, Grid, Paper,
    Typography
} from '@mui/material';
import formulario from '../store/formularioColecciones';
import useDataStoreFCol from '../data/dataColeccionesF';
import useDataColecciones from '../data/dataColecciones';
import getColecciones from '../api/getColecciones';
import putColecciones from '../api/putColecciones';
import { ToastContainer, toast } from 'react-toastify';
import CircularProgress from '@mui/material/CircularProgress';


/**
 * 
 * Funcion que regresa el formulario de colecciones
 * 
 */
const ColumnaFormularioColecciones = () => {

    /**
     * constante para guardar los datos de las colecciones
     */
    const [colecciones, setColecciones] = useState([])
    const [loader, setLoader] = useState(false);
    const [loaderMod, setLoaderMod] = useState(false);
    const [loaderDel, setLoaderDel] = useState(false);

    /**
     * 
     * Uso del use effect para ejecutar el codigo al inicio
     * 
     */
    React.useEffect(() => {
        datos();
    }, [])


    /**
      * 
      * Constantes para guardar la informacion de las colecciones y ser compartida con el 
      * componente vista para visualizar los cambios
      */
    const setSharedData = useDataColecciones((state) => state.setSharedData);
    const sharedDataColecciones = useDataColecciones((state) => state.sharedData);

    /**
     * 
     * Funcion para conseguir los datos
     * 
     */
    const datos = async () => {
        const res = await getColecciones();
        console.log('[columnaFormularioColecciones][resultado]', res.resultado);
        setSharedData(res.resultado);
        setColecciones(res.resultado);
    }



    /**
     * 
     * Destructuracion de datos de formulario
     * 
     */
    const { idColeccion, idSubColeccion, nombre, url } = formulario(state => state);



    /**
     * 
     * Constante de los datos del formulario
     * 
     */
    const [formData, setFormData] = useState({
        idSubColeccion: idSubColeccion,
        idColeccion: idColeccion,
        url: url,
        nombre: nombre
    });

    /**
     * 
     * Asignacion de la data compartida entre los banners y formularios
     * 
     */
    const sharedData = useDataStoreFCol((state) => state.sharedData);
    console.log('[columnaFormularioColecciones][sharedData]', sharedData)


    /**
     * 
     * Use Effect para actualizar informacion en cada refresh
     */
    useEffect(() => {

        if (sharedDataColecciones === '') {
            datos();
        } else {
            setSharedData(colecciones)
        }

        setFormData({
            idColeccion: sharedData?.idColeccion ?? '',
            idSubColeccion: sharedData?.idSubColeccion ?? '',
            url: sharedData?.url ?? '',
            nombre: sharedData?.nombre ?? '',
        })


    }, [sharedData, sharedDataColecciones]);


    /**
     * 
     * Funcion para actualizar los datos del formualrio
     * 
     */
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };


    /**
     * 
     * Funcion para guardar la informacion
     * 
     */
    const handleSubmit = async (event) => {
        try {
            setLoader(true);
            console.log('[columnaFormularioColecciones][handleSubmit][iniciando] . .');
            event.preventDefault();
            console.log('[columnaFormularioColecciones][handleSubmit][formData]', formData);
            await putColecciones("1", formData.idColeccion, formData.idSubColeccion, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
            setSharedData({});
        } catch (error) {
            console.error('[columnaFormularioColecciones][handleSubmit][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    };


    /**
     * 
     * Funcion para eliminar la informacion
     * 
     */
    const handleEliminar = async (event) => {
        try {
            setLoaderDel(true);
            console.log('[columnaFormularioColecciones][handleEliminar][iniciando] . .');
            event.preventDefault();
            await putColecciones("3", formData.idColeccion, formData.idSubColeccion, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoaderDel(false);
        } catch (error) {
            console.error('[columnaFormularioColecciones][handleEliminar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoaderDel(false);
        }
    }



    const handleModificar = async (event) => {
        try {
            setLoaderMod(true);
            console.error('[columnaFormularioColecciones][handleModificar][iniciando] . .');
            event.preventDefault();
            console.log('[columnaFormularioColecciones][handleModificar][formData]', formData);
            await putColecciones("2", formData.idColeccion, formData.idSubColeccion, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoaderMod(false);
        } catch (error) {
            console.error('[columnaFormularioColecciones][handleModificar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    }



    return (
        <Grid item xs={6}>
            <ToastContainer />
            <Paper elevation={3} style={{ padding: 20 }}>
                <Typography variant="h5" align="center" gutterBottom>
                    Colecciones
                </Typography>
                <form onSubmit={handleSubmit}>
                    <TextField
                        label="ID Coleccion"
                        name="idColeccion"
                        fullWidth
                        value={formData.idColeccion}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <TextField
                        label="ID SubColeccion"
                        name="idSubColeccion"
                        fullWidth
                        value={formData.idSubColeccion}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <TextField
                        label="Nombre"
                        name="nombre"
                        fullWidth
                        value={formData.nombre}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <TextField
                        label="URL"
                        name="url"
                        fullWidth
                        value={formData.url}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <Button fullWidth type="submit" variant="contained" color="primary">
                        {loader ? <CircularProgress size={24} color="inherit" /> : 'Crear'}
                    </Button>
                </form>
                <br />
                <Button fullWidth onClick={handleModificar} variant="contained" color="secondary" >
                    {loaderMod ? <CircularProgress size={24} color="inherit" /> : 'Modificar'}
                </Button>
                <br />
                <br />
                <Button fullWidth onClick={handleEliminar} variant="contained" color="error">
                    {loaderDel ? <CircularProgress size={24} color="inherit" /> : 'Eliminar'}
                </Button>
            </Paper>
        </Grid>
    );
}

export default ColumnaFormularioColecciones;