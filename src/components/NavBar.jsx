import { AppBar, Toolbar, Typography, Button } from '@mui/material';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <AppBar position="static" style={{ backgroundColor: '#FF0000' }}>
            <Toolbar>
                <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    EKT
                </Typography>
                <Button component={Link} to="/" color="inherit">
                    Home
                </Button>
                <Button component={Link} to="/categorias" color="inherit">
                    Categorias
                </Button>
                <Button component={Link} to="/plp" color="inherit">
                    PLP
                </Button>
                <Button component={Link} to="/listaC" color="inherit">
                    Lista
                </Button>
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;