/* eslint-disable no-case-declarations */
/**
 * 
 * 
 * Importaciones
 * 
 */
import React, { useState, useEffect } from "react";
import '../App.css';
import {
    TextField, Button, Grid, Paper,
    Typography
} from '@mui/material';
import formulario from '../store/formularioListaC';
import useDataStoreFCatL from '../data/dataListaCatF';
import useDataCat from '../data/dataListaCat';
import getCategoriasLista from '../api/getCategoriasLista';
import putCategoriasLista from '../api/putCategoriasLista';
import { ToastContainer, toast } from 'react-toastify';
import CircularProgress from '@mui/material/CircularProgress';


/**
 * 
 * Funcion para retornar el formualrio de las
 * lista de categorias
 * 
 */
const ColumnaFormularioCatL = () => {

    /**
     * 
     * Funcion para almacenar la lista de categorias
     * 
     */
    const [catLista, setCatLista] = useState([])
    const [loader, setLoader] = useState(false);
    const [loaderMod, setLoaderMod] = useState(false);
    const [loaderDel, setLoaderDel] = useState(false);

    /**
     * 
     * UseEffect para actualizar info al iniciar
     * 
     */
    React.useEffect(() => {
        datos();
    }, [])


    /**
     * 
     * Constantes para guardar la informacion de las categorias y ser compartida con el 
     * componente vista para visualizar los cambios
     */
    const setSharedData = useDataCat((state) => state.setSharedData);
    const sharedDataCatLista = useDataCat((state) => state.sharedData);

    /**
     * 
     * Funcion para conseguir los datos de la categorias
     * 
     */
    const datos = async () => {
        console.log('[columnaFormularioCategoriasLista][datos][iniciando] . .');
        const res = await getCategoriasLista();
        console.log('[columnaFormularioCategoriasLista][datos][res]', res.resultado);
        setSharedData(res.resultado);
        setCatLista(res.resultado);
    }



    /**
     * 
     * Destructuracion de datos de formulario
     * 
     */
    const { id, nombre, url } = formulario(state => state);

    /**
     * 
     * Constante de los datos del formulario
     * 
     */
    const [formData, setFormData] = useState({
        id: id,
        url: url,
        nombre: nombre
    });

    /**
     * 
     * Asignacion de la data compartida entre las cateoiras de lista  y formularios
     * 
     */
    const sharedData = useDataStoreFCatL((state) => state.sharedData);
    console.log('[columnaFormularioCategoriasLista][sharedData]', sharedData);


    /**
     * 
     * Use Effect para actualizar informacion en cada refresh
     */
    useEffect(() => {

        if (sharedDataCatLista === '') {
            datos();
        } else {
            setSharedData(catLista)
        }

        setFormData({
            id: sharedData?.id ?? '',
            url: sharedData?.url ?? '',
            nombre: sharedData?.nombre ?? '',
        })


    }, [sharedData, sharedDataCatLista]);


    /**
     * 
     * Funcion para actualizar los datos del formualrio
     * 
     */
    const handleInputChange = (event) => {
        const { name, value } = event.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    /**
     * 
     * Funcion para guardar la informacion
     * 
     */
    const handleSubmit = async (event) => {
        try {
            setLoader(true);
            console.log('[columnaFormularioCategoriasLista][handleSubmit][inciando] . .');
            event.preventDefault();
            console.log('[columnaFormularioCategoriasLista][handleSubmit][formData]', formData);
            await putCategoriasLista("1", formData.id, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
            setSharedData({});
        } catch (error) {
            console.error('[columnaFormularioCategoriasLista][handleSubmit][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    };


    /**
     * 
     * Funcion para eliminar informacion
     * 
     */
    const handleEliminar = async (event) => {
        try {
            setLoaderDel(true);
            console.log('[columnaFormularioCategoriasLista][handleEliminar][inciando] . .');
            event.preventDefault();
            console.log('[columnaFormularioCategoriasLista][handleEliminar][formData]', formData);
            await putCategoriasLista("3", formData.id, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoaderDel(false);
        } catch (error) {
            console.error('[columnaFormularioCategoriasLista][handleEliminar][error]', error);
            console.error('[columnaFormularioColecciones][handleEliminar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoaderDel(false);
        }
    }


    /**
     * 
     * Funcion para modificar la informacion
     * 
     */
    const handleModificar = async (event) => {
        try {
            setLoaderMod(true);
            console.log('[columnaFormularioCategoriasLista][handleModificar][inciando] . .');
            event.preventDefault();
            console.log('datos a enviar', formData);
            console.log(formData);
            await putCategoriasLista("2", formData.id, formData.nombre, formData.url)
            await datos();
            toast.success("Operación Exitosa", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setSharedData({});
            setLoaderMod(false);
        } catch (error) {
            console.error('[columnaFormularioCategoriasLista][handleModificar][error]', error);
            toast.error("Operación Fallida", {
                position: toast.POSITION.TOP_RIGHT,
            });
            setLoader(false);
        }
    }


    return (
        <Grid item xs={6}>
            <ToastContainer />
            <Paper elevation={3} style={{ padding: 20 }}>
                <Typography variant="h5" align="center" gutterBottom>
                    Categorias
                </Typography>
                <form onSubmit={handleSubmit}>
                    <TextField
                        label="ID"
                        name="id"
                        fullWidth
                        value={formData.id}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <TextField
                        label="Nombre"
                        name="nombre"
                        fullWidth
                        value={formData.nombre}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <TextField
                        label="URL"
                        name="url"
                        fullWidth
                        value={formData.url}
                        onChange={handleInputChange}
                        margin="normal"
                    />

                    <Button fullWidth type="submit" variant="contained" color="primary">
                        {loader ? <CircularProgress size={24} color="inherit" /> : 'Crear'}
                    </Button>
                </form>
                <br />
                <Button fullWidth onClick={handleModificar} variant="contained" color="secondary" >
                    {loaderMod ? <CircularProgress size={24} color="inherit" /> : 'Modificar'}
                </Button>
                <br />
                <br />
                <Button fullWidth onClick={handleEliminar} variant="contained" color="error">
                    {loaderDel ? <CircularProgress size={24} color="inherit" /> : 'Eliminar'}
                </Button>
            </Paper>
        </Grid>
    );
}

export default ColumnaFormularioCatL;